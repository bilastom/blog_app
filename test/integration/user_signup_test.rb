require 'test_helper'

class UserSignupTest < ActionDispatch::IntegrationTest

  def setup
    @user = User.create( 
      username: "testuser", 
      email: "testuser@example.com", 
      password: "iamthepassword"
    )
  end

  test "test login user" do
    get login_path
    assert_template 'sessions/new'
    post login_path, session: { email: "testuser@example.com", password: "iamthepassword"}
    assert_redirected_to user_path( @user )
  end

end